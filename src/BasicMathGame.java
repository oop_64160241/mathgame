import java.util.Scanner;

public class BasicMathGame {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double num1, num2, ans, result = 0;
        boolean correct = true;
        char operator[] = {'+', '-', '*', '/'};
        char op;
        int id, score = 0;
        while (correct) {
            num1 = (int) (1 + (Math.random() * 12));
            num2 = (int) (1 + (Math.random() * 12));
            id = (int) (Math.random() * 4);
            op = operator[id];
            System.out.println(num1 + " " + op + " " + num2);
            System.out.print("Enter answer: ");
            ans = scan.nextDouble();
            switch (op) {
                case '+':
                    result = num1 + num2;
                    break;
                case '-':
                    result = num1 - num2;
                    break;
                case '*':
                    result = num1 * num2;
                    break;
                case '/':
                    result = num1 / num2;
                    break;
            }
            if (result == ans) {
                correct = true;
                System.out.println("Correct! Good job.");
                score += 1;
            } else {
                correct = false;
                System.out.println("Incorrect Game Over.");
            }
        }
        System.out.println("Score : " + score);
    }
}